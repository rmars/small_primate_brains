#!/usr/bin/env bash
umask u+rw,g+rw # give group read/write permissions to all new files
set -e    # stop immediately on error


main_root=/your_path/

script_path=$main_root/scripts/Analysis

species="lemur squirrel_monkey macaque"

hemis="l r"


for sp in $species; do
	# Tracto folder
	tracto_root=$main_root/$sp/tractography/average/surface/
	surface_root=$main_root/$sp/surface
	for h in $hemis; do

		blueprint=$tracto_root/tracts_surf.${h}.dtseries.nii

		if [[ "$h" = "l" ]]; then

			$WBDIR/wb_command -cifti-smoothing $blueprint 1 1 COLUMN $tracto_root/tracts_surf_smooth.${h}.dtseries.nii \
			 -left-surface $surface_root/lh.midthickness.10K.surf.gii

		else 

			$WBDIR/wb_command -cifti-smoothing $blueprint 1 1 COLUMN $tracto_root/tracts_surf_smooth.${h}.dtseries.nii \
			 -right-surface $surface_root/rh.midthickness.10K.surf.gii
		fi

		$MATLABBIN/matlab -nodisplay -nosplash -r "addpath('$script_path'); lognorm_surf_tract('$main_root','$sp','$h'); exit"

		$WBDIR/wb_command -cifti-label-import $tracto_root/tracts_surf_smooth_lognorm_thr08.${h}.dtseries.nii \
		$script_path/labels_list.txt \
		$tracto_root/tracts_surf_smooth_lognorm_thr08_label.${h}.dlabel.nii


	done
done
