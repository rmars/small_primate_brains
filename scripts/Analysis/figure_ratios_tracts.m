%% tract ratios calculation

%main path
main_folder = '/your_path';

species = {'lemur','squirrel_monkey','macaque'};
species_name={'L','S','M'};

tract_asso = {'slfc','or','lft','mdlf','ilf','unc'}; % tracts to compare
tract_ratio ={'cbd','cbt','cbt','cbt','cbt','cbt'}; % tracts to compare with in same order

tract_names = {'SLFc','OR','LFT','MdLF','ILF','UNC'};
CB_names = {'CBd','CBt','CBt','CBt','CBt','CBt'};

%hemi
hemis={'l','r'};
hemis_name={'L','R'};

%initialisation
my_summary_measure=zeros(length(tract_asso), length(species),length(hemis)); %col species
ratios=zeros(length(tract_asso), length(species),length(hemis)); %col species

sb=0;

for tr = 1:length(tract_ratio)
    tract_r = tract_ratio{tr};
    tract = tract_asso{tr};
    t_name = tract_names{tr};
    cb_name = CB_names{tr};
    switch tract
        case 'slfc'
            mycol = [1 0.33 0.68];
        case 'lft'
            mycol = [0.68 0.4 1];
        case 'or'
            mycol = [0     0.7     0];
        case 'ilf'
            mycol = [0.68  0.5 0];
        case 'mdlf'
            mycol = [0 0.6 1];
        case 'unc'
            mycol = [0 0.67 0.34];
    end
    for h = 1:length(hemis)
        hemi=hemis{h};
        sb=sb+1;

        for ss=1:length(species)
            specie = species{ss};
            %get white matter mask
            WM_mask=[main_folder, specie, '/WM/wm_mask_',hemi,'.nii.gz'];  
            WM = readimgfile(WM_mask); WM = WM(:);
            
            %get tract to compare
            densityNorm=[main_folder, specie, '/tractography/average/', tract, '/',tract, '_',hemi,'_lognorm.nii.gz'];
            data = readimgfile(densityNorm); data = data(:); 
            data(data(:)<0.8)=0; %threshold
            my_summary_measure(tr,ss,h) = sum(data.*WM); %mask with white matter
            
            %get tracts to compare with
            densityNorm_ratio=[main_folder, specie, '/tractography/average/', tract_r, '/',tract_r, '_',hemi,'_lognorm.nii.gz'];                
            data_ratio = readimgfile(densityNorm_ratio); data_ratio = data_ratio(:); 
            data_ratio(data_ratio(:)<0.8)=0; %threshold
            my_summary_ratio = sum(data_ratio.*WM); %mask with white matter
            
            %comparison
            ratios(tr,ss,h)=my_summary_measure(tr,ss)/my_summary_ratio;

        end


        subplot_tight(1,length(tract_asso)*length(hemis),sb,[0.2 0.03]);
        bar(ratios(tr,:,h),'EdgeColor','none', 'FaceColor', mycol)

        axy = [0.5 3.5 0 10 ]; %10
        axis(axy)
        yt=0:5:10;
        if sb == 1
            set(gca,'xticklabel',species_name,'ytick',yt,'FontSize',10)
        else
            set(gca,'xticklabel',[],'yticklabel',[])
        end
        title([t_name '/' cb_name],'FontSize', 7)

    end
end

set(gcf,'units', 'centimeters','position',[0 0 18 7],'color','w')
figname = [main_folder '/figures/tractography/ratios_tracts.png'];
saveas(gcf,figname)


