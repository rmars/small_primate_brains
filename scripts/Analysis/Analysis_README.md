# Cortical morphology and white matter tractography of three phylogenetically distant primates: Evidence for a simian elaboration

Scripts in this repository will need preprocessed data output such as bedpostX folder and warp from subject space to standard space.

# Tractography

**1. Protocols**

Each folder contains:
1 seed, 1 target, 1 exclude and 1 invert file.
ILF and MdLF test are for the control analysis.

relevant scripts to make the protocol figures: ```figure_tractography_recipes.sh``` (*figure 1*)
(calls convert from imagemagick)

**2. Run xtract**

Files needed in tractography folder of each species: ```slist.txt``` file with all the tracts you want in format:
tract_l 10
tract_r 10
...
And ```options.txt``` with the options that you want to change from probtrackx

relevant scripts: ```wrapper_xtract.sh```
data needed: BedpostX data, wrap from standard to subject space and form subject to standard space

**3. Average the tracts across subject**

relevant scripts: ```average_tracts.sh```

Average tracts, log norm them and copy the results to the /average/all folder.


# Controls

**1. ILF parietal branch control**

protocols found in protocols folder with name 'test'

relevant scripts: ```figure_tractography_recipes_ilfcontrol.sh``` to show the recipe figures (*figure 2*)

Then ```wrapper_xtract.sh``` with a different slist to run the tractography for ilf_test and mdlf_test

Then ```figure_control_ilf.m``` to make the ratio and the figure. (uses individual data and boxplot_statistics.m script) (*figure 6C*)


**2. SLFc and OR control**

relevant scripts: ```figure_ratios_tracts.m``` (uses subplot_tight function) wich looks at ratio with according tracts (cbd or cbt) and then save figure (*figure 8C*)  

# Surface tracts

**1. Create the connectivity matrix**

relevant scripts: ```create_surface_fdtmatrix2.sh```
(calls quicktract_gpu)

data needed: bedpostX folder with nodif_brain_mask, standard images, warps from standard space to diffusion space and inversly and a surface midthickness for each species and a medial wall (named midline) mask.

This step creates a matrix defining the connectivity of each vertex to the GM to the rest of the brain.

**2. Average fdt_matrix2**

relevant scripts: ```average_fdt_matrix2.m```

This step average the matrices obtained at the previous stages across subjects.

**3. Create the surface tracts**

relevant scripts: ```create_surf_tract.m``` uses the multiply_fdt from MrCatDev

relevant files: in tractography/average and averaged matrices from previous step.

This step creates the surface tracts in a dtseries in tractography/average/surface/tract_surf.hemi.dtseries.nii

**4. Prepare the files to make a good workbench scenes**

Need to define a label list with the colors that you want (saved in script folder)
Ex:
```
CBd
1 255 94 87 255
Name
value of the tract in your dtseries / RGB code / alpha
```

relevant scripts: ```surf_tracts_smoothloglabel.sh``` (calls lognorm_surf_tract.m)

Then open the scene file: $WBDIR/wb_view figures/tractography/all_tracts_surface.scene and choose which tract (*figures 5B, 6B, 7B, 8B*)

**5. Tracts and cortical labels**

In scene file all_tracts_surface.scene and open scene slfc_or_labels (*figure 9*).

