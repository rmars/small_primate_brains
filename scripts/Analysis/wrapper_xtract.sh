#!/usr/bin/env bash
umask u+rw,g+rw # give group read/write permissions to all new files
set -e    # stop immediately on error

main_root=/your_path/ # to data
scratch_root=/path_to_output/ #to output folders if different

species="lemur squirrel_monkey macaque"

subjects='01 02 03'

for sp in $species; do

	for subj in $subjects;do
		subjName=subject_${subj}
		subj_folder=$main_root/$sp/subjName

		bedpost=$subj_folder/acpc/dMRI.bedpostX
		structureList=$scratch_root/$sp/tractography/slist
		optionsList=$scratch_root/$sp/tractography/options.txt

		dif2std=$scratch_root/$sp/tractography/stdwarp/$subjName/diff2stand.nii.gz
		std2dif=$scratch_root/$sp/tractography/stdwarp/$subjName/stand2diff.nii.gz

		xtract -bpx $bedpost \
		-out $scratch_root/$sp/tractography/tracts/$subjName \
		-species MACAQUE \
		-str $structureList \
		-p $scratch_root/$sp/tractography/protocols/ \
		-stdwarp $std2dif $dif2std \
		-ptx_options $optionsList \
		-gpu

	done

done

