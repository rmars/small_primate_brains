%% Create blueprint for macaques and humans

%% ----------------
% edit with your own path
main_folder =  '/your_path'; % to the big matrices
scratch_folder = '/path_to_output/'; %to output folders if different
%% ----------------

%% Loop trough species, hemispheres
species = {'lemur','squirrel_monkey','macaque'};

hemis = {'l','r'};
Hemis = {'L', 'R'};

for s = 1:length(species) % Loop trough species
    specie = species{s};

    resultsDir = [main_folder specie '/blueprint/'];
    for h = 1:length(hemis) % Loop trough hemis
        hemi = hemis{h};
        Hemi = Hemis{h};
        tractDir = [scratch_folder specie '/tractography/average/'];
            
        multiply_fdt('fdt_matrix2',[resultsDir '/AVG_' specie '_Matrix_cortexbrain_' hemi '.mat'],...
       'fdt_paths',[tractDir 'cbd/cbd_' hemi '.nii.gz'],...
       'fdt_paths',[tractDir 'cbp/cbp_' hemi '.nii.gz'],...
       'fdt_paths',[tractDir 'cbt/cbt_' hemi '.nii.gz'],...
       'fdt_paths',[tractDir 'ilf/ilf_' hemi '.nii.gz'],...
       'fdt_paths',[tractDir 'lft/lft_' hemi '.nii.gz'],...
       'fdt_paths',[tractDir 'mdlf/mdlf_' hemi '.nii.gz'],...
       'fdt_paths',[tractDir 'or/or_' hemi '.nii.gz'],...
       'fdt_paths',[tractDir 'slfc/slfc_' hemi '.nii.gz'],...
       'fdt_paths',[tractDir 'unc/unc_' hemi '.nii.gz'],...
       'outputname',[tractDir '/surface/tracts_surf.'  hemi '.dtseries.nii'],'hemi',Hemi,...
       'mask',[scratch_folder,specie,'/standard/mask.nii.gz'],...
       'normalise', 'no',...
       'eco', 'yes');

    end %hemi loop
end % species loop
