#!/bin/bash
umask u+rw,g+rw # give group read/write permissions to all new files
set -e    # stop immediately on error

#### ---------------
# edit with your own path
main_root=/your_path/ # to data
scratch_root=/path_to_output/ #to output folders if different
#### ---------------

subjects='01 02 03'

# Loop trough species and hemispheres and subjects
species='lemur squirrel_monkey macaque'


hemis='l r'

for sp in $species; do # loop trough species
	# define for each species the standard space, the subjects name and surface

	std=$scratch_root/$sp/standard/t2_template

	for subj in $subjects;do
		subjName=subject_${subj}
		subj_folder=$main_root/$sp/subjName

		bedpostDir=$subj_folder/acpc/dMRI.bedpostX #bedpost directory
		std2dMRI=$scratch_root/$sp/tractography/stdwarp/$subjName/stand2diff.nii.gz #warp from standard space to diffusion space
		dMRI2std=$scratch_root/$sp/tractography/stdwarp/$subjName/diff2stand.nii.gz #warp from diffusion space to standard space

		for h in $hemis; do
			surf=$scratch_root/$sp/surface/${h}h.midthickness.10K.surf.gii
			resultsDir=$main_root/$sp/blueprint/subjects/$subjName/surf_seed_${h}
			mkdir -p $resultsDir

			midline=$scratch_root/$sp/surface/midline_${h}.func.gii
			
			
			/vols/Data/rbmars/Chimps/scripts/quicktrack_gpu $bedpostDir $resultsDir \
				--xfm=$std2dMRI \
				--invxfm=$dMRI2std \
				--seed=$surf \
				--omatrix2 \
				--target2=${std}_brain \
				--stop=$midline \
				-P 10000 \
				--seedref=$std
	     

		done #hemis

	done < 

done #species
