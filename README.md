# project_primates_comps

This repository contains processing code and result files associated with the following publication: 

## Cortical morphology and white matter tractography of three phylogenetically distant primates: Evidence for a simian elaboration

### Lea Roumazeilles, Frederik J. Lange, R. Austin Benn, Jesper L. R. Andersson, Mads Frost Bertelsen, Paul R Manger, Edmund Flach, Alexandre A. Khrapitchev, Katherine L. Bryant, Jérôme Sallet, Rogier B. Mars

Explanations to use the scripts used for the results can be found in scripts/Analysis and scripts/Template
Some scripts call other scripts from MrCat toolbox available at: https://github.com/neuroecology/MrCat

Raw data of the lemur and squirrel monkey will be made available on the Digital Brain Bank of the Wellcome Centre for Integrative Neuroimaging (https://open.win.ox.ac.uk/DigitalBrainBank/#/) upon acceptance of the paper. Macaque data are already available within PRIME-DE ((Milham et al. 2018); http://fcon_1000.projects.nitrc.org/indi/indiPRIME.html). The scripts used for preprocessing are available in MrCat (www.neuroecologylab.org, https://github.com/neuroecology/MrCat). MMORF is available as a Singularity image online (https://git.fmrib.ox.ac.uk/flange/mmorf_beta). The surface pipeline is available online (https://github.com/neurabenn/precon_all).


## Repository folder organisation

Species could be: lemur, squirrel_monkey, macaque

Repository is organised organised as follow: 
```
figures 
	|
	+-- recipes (figures will be created with the scripts provided)
	|
	+-- templates (scene file to make *figure 3*)
	|
	+-- tractography (scene file to make figure *5-8B*)

scripts
	|
	+-- Analysis (with Analysis_README.md)
	|
	+-- Template (with templating_README.md)

species 
	|
	+-- standard: t2_template, dti_iter_5b (both outputs of the template pipeline), mask for these images, dti_FA and dyads (average FA and principal direction in template space)
	|
	+-- surface: midthickness and pial surface, normal and downsample for both hemisphere, medial wall mask (named midline), and visual, motor, frontal and granular masks
	|
	+-- tractography 
		|
		+-- average tracts across subjects and surface average
		|
		+-- protocols (tractography recipes, ilf and mdlf 'test' are for the control analysis)
```